import sys
sys.path.append('~/Library/Frameworks/Python.framework/Versions/3.10/lib/python3.10/site-packages') #import selenium from this directory 
import time
from selenium import webdriver
from selenium.webdriver.common.by import By
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC

#Set the mobileEmulation and enter the home page
mobile_emulation = { "deviceName": "Pixel 5" }
chrome_options = webdriver.ChromeOptions()
chrome_options.add_experimental_option("mobileEmulation", mobile_emulation)
driver = webdriver.Chrome('./chromedriver', options=chrome_options)
driver.get('https://www.cathaybk.com.tw/cathaybk/')

#WebDriverWeit until function
#Wait element that is clickable
def WaitSingleElementClickable(WaitSec, xpath):
    a = WebDriverWait(driver, WaitSec).until(EC.element_to_be_clickable((By.XPATH, xpath)))
    return a

#Wait element to be presence
def WaitSingleElementPresence(WaitSec, xpath):
    a = WebDriverWait(driver, WaitSec).until(EC.presence_of_element_located((By.XPATH, xpath)))
    return a

#Wait all elements to be presence
def WaitAlllElementPresence(WaitSec, xpath):
    a = WebDriverWait(driver, WaitSec).until(EC.presence_of_all_elements_located((By.XPATH, xpath)))
    return a


#Find the buger menu and click 
try:
    home_manu = WaitSingleElementClickable(10, "//*[@class='cubre-a-burger']")
except:
    print("Cannot find home_manu")
    driver.quit()
else:
    home_manu.click()

#Find and clock the 個人金融 > 產品介紹
try:
    personal_finance_product_intro = WaitSingleElementClickable(10, "//*[contains(text(), '產品介紹')]")
except:
    print("Cannot find personal_finance_product_intro")
    driver.quit()
else:
    personal_finance_product_intro.click()

#Find the 信用卡 and click it
try:
    personal_finance_product_CC_btn = WaitSingleElementClickable(10, "//*[contains(text(), '信用卡')]")
except:
    print("Cannot find personal_finance_product_CC_btn")
    driver.quit()
else: 
    personal_finance_product_CC_btn.click()

#Result 1
#Find the 信用卡 and print the lenght of the list
try: 
    personal_finance_product_CC_list = WaitAlllElementPresence(10, "//*[contains(text(), '信用卡') and @class='cubre-a-menuSortBtn cubre-u-mbOnly']/following-sibling::a")
except:
    print("Cannot find personal_finance_product_CC_list")
    driver.quit()
else:
    print(f"The elements under 信用卡 submenu: {len(personal_finance_product_CC_list)}")

#Find the 卡片介紹 and click it, redirect to another page
try:
    personal_finance_product_CC_intro = WaitSingleElementClickable(10, "//*[contains(text(), '卡片介紹')]")
except:
    print("Cannot find personal_finance_product_CC_intro")
    driver.quit()
else: 
    personal_finance_product_CC_intro.click()

#Find the content
try:
    int_card_number = WaitSingleElementPresence(10, "//*[@id='layout_0_content_0_updatepanel_0_lbCardsList__amount']")
    int_card_number_result = int(int_card_number.text)
except:
    print("Cannot find int_card_number")
    driver.quit()

#Find the credit card conent
try:
    block_card_number = WaitAlllElementPresence(10, "//*[@class='cardsBox border']")
    block_card_number_result = len(block_card_number)
except:
    print("Cannot find block_card_number")
    driver.quit()

#Result 2
#May improve with unittest
if int_card_number_result != block_card_number_result:
    print("Fail, The number of cart counter does NOT MATCH number of card block")
    print (f"number of counter: {int_card_number_result}\nnumber of blocks: {block_card_number_result}" )
else:
    print("Pass, The number of cart counter does MATCH number of card block")


#Wait 5 sec for observation
#Close the driver
time.sleep(5)
driver.quit()
